/* 
 * File:   LedListner.hpp
 * Author: romi
 *
 * Created on September 24, 2015, 9:59 PM
 */

#ifndef LEDLISTNER_HPP
#define	LEDLISTNER_HPP
#include "Commands.hpp"
namespace leds{
class LedListner
{
    public: virtual void
    led_monitorresult(Command command, const exception& e) = 0;
};
}

#endif	/* LEDLISTNER_HPP */

