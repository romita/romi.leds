/* 
 * File:   Monior.cpp
 * Author: romi
 *
 * Created on June 28, 2015, 9:27 PM
 */

#include <iostream>
#include "Monitor.hpp"

using namespace std;

void
Monitor::notify_all()
{
    cv.notify_all();
}
void
Monitor::notify_one()
{
    cv.notify_one();
}
void
Monitor::wait(unique_lock<mutex>& lock)
{
    cv.wait(lock);
}
