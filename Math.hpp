/* 
 * File:   linked_list.hpp
 * Author: romi
 *
 * Created on June 29, 2015, 9:54 PM
 */
/*#ifdef _cplusplus*/
#ifndef MATH_HPP
#define	MATH_HPP
#include <list>
#include "MathListener.hpp"
#include "Monitor.hpp"
#include <mutex>
#include <condition_variable> 
using namespace std;
namespace leds {
class Math
{
    std::list<MathListener*> mathlisteners;
    public:Math();
    public: void
    addListener(MathListener* mathlistener);
    private: static void*
    worker(void* const data);
    private: int working;
    private: class MathRequest
    {
        public: int a;
        public: int b;
        MathListener* mathlistener;
        MathRequest(int a,int b,MathListener* mathlistener);
    };
    std::list<MathRequest*> mathrequests;
    private: Monitor* monitor; 
    public: void add(int a, int b, MathListener* mathlistener);
    public: int add(int a, int b);
    
};

} //leds

#endif	/* LINKED_LIST_HPP */


