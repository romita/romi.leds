/* 
 * File:   Monitor.hpp
 * Author: romi
 *
 * Created on September 8, 2015, 10:36 PM
 */

#ifndef MONITOR_HPP
#define	MONITOR_HPP

#include <mutex> 
#include <condition_variable> 
using namespace std;


class Monitor:
    public mutex
{
    private:condition_variable cv;
    public:void notify_all();
    public:void notify_one();
    public:void wait(unique_lock<mutex>& lock);
};


#endif	/* MONITOR_HPP */

