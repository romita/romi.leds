/* 
 * File:   led_monitor.cpp
 * Author: romi
 *
 * Created on June 28, 2015, 9:27 PM
 */

#include <string>
#include <iostream>
#include "Led_monitor.hpp"
#include "pthread.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#define STATUS_SUCCESS 0
#define STATUS_FAILURE 1
using namespace std;
namespace leds {
    
Pipe::Pipe():
closed(false)
{
    cout << "Pipe constructor" << endl;
    {
        descriptors[INDEX_READ] = DESCRIPTOR_INVALID;
        descriptors[INDEX_WRITE] = DESCRIPTOR_INVALID;
        
        int retval = ::pipe(descriptors);
        cout << "Value of return from pipe initialization" << retval << endl;
        if (0 != retval)
        {
            throw runtime_error(string("Failed to create the pipe with error: ") + ::strerror(errno));
        }
        
        assert(DESCRIPTOR_INVALID != descriptors[INDEX_READ]);
        assert(DESCRIPTOR_INVALID != descriptors[INDEX_WRITE]);
    }
}
Pipe::~Pipe()
{
    close();
}
void
Pipe::close()
{
    {
        if (!closed)
        {
            int retval = ::close(descriptors[INDEX_READ]);
            
            if (0 != retval)
            {
                cerr << "Failed to close the read descriptor with error: " << ::strerror(errno) << endl;
            }
            retval = ::close(descriptors[INDEX_WRITE]);
            if (0 != retval)
            {
                cerr << "Failed to close the write descriptor with error: " << ::strerror(errno) << endl;
            }

            if(EINTR != retval)
            { 
                closed = true;
            }
            else
            {
                throw runtime_error("Run time error:close operation got interrupted by a signal");
            }
        }
    }
    }

int
Pipe::getReadDescriptor()
{
    return descriptors[INDEX_READ];
}
int
Pipe::getWriteDescriptor()
{
    return descriptors[INDEX_WRITE];
}
    
Led_monitor::Led_monitor():   // initialization list initializes member of any class by calling its default constructor.Monitor's constructor will be called, initialize variable,pointers here.
currentState(STATE_OFF),working(1),thread_worker(0),thread_notify(0),pipeNotify()
{
    
}
const int Led_monitor::FILE_DELETE;
const int Led_monitor::FILE_MODIFY;
const int Led_monitor::FILE_CLOSE;

void*
Led_monitor::worker_notify(void* const context)
{
    localize(context).worker_notify();
    return 0;
}

Led_monitor&
Led_monitor::localize(void* const context)
{
    assert(0 != context);
    return *static_cast<Led_monitor*>(context);
}


Pointer<Led_monitor>
Led_monitor::create()
{
    Pointer<Led_monitor> ledMonitor = new Led_monitor();
    int fileFd = open("/sys/class/leds/phy0-led/brightness", O_RDONLY);
    if(-1 == fileFd)
    {
        throw(ios_base::failure("Error in opening the file"));
    }
    char string[2];
    size_t length = sizeof(string);
    size_t sum = 0;
    size_t read_bytes = 0;
    while(sum != length)
    {
        read_bytes = read(fileFd,string,length-sum);
        
        sum += read_bytes;
    }
    
    if(length == sum)
    {
        ledMonitor->currentState = (0 == string[0])? STATE_OFF:STATE_ON;
        ledMonitor->fd = inotify_init1(IN_NONBLOCK);
        if(-1 != ledMonitor->fd)
        {
            ledMonitor->wd = inotify_add_watch(ledMonitor->fd, "/sys/class/leds/phy0-led/brightness", IN_DELETE | IN_MODIFY);
            if(-1 == ledMonitor->wd)
            {
                cout << "Error in adding watch list: " << strerror(errno) << endl;
            }
        }
    }
    cout << "Creating worker thread" << endl;
    int retval = 0;
    retval = pthread_create(&ledMonitor->thread_worker,NULL,worker,ledMonitor.operator->());
    if(0 == retval)
    {
        retval = pthread_create(&ledMonitor->thread_notify,NULL,worker_notify,ledMonitor.operator ->());
        if(0 != retval)
        {
            cerr << "Error in creating the worker notify thread" << endl;
            ledMonitor->LedMonitorClose();
            
        }
    }
    if(0 != retval)
    {
        throw runtime_error("Error occured while creating threads");
    }
    return ledMonitor;
}

void
Led_monitor::addListner(LedListner* ledlistner)
{
    if(0 != ledlistner)
    {
        this->ledlistners.push_back(ledlistner);
    }
}

Led_monitor::StateChangeRequest*
Led_monitor::StateChangeRequest::REQUEST_CLOSE()
{
    static StateChangeRequest stateChangeRequest = StateChangeRequest(STATE_NONE, 0);
    
    return &stateChangeRequest;
}
    
const exception&
Led_monitor::EXCEPTION_HANDLE()
{
    static const exception e;
    
    return e;
}

    
Led_monitor::StateChangeRequest::StateChangeRequest(Command commands, LedListner* ledlistner):
commands(commands),ledlistner(ledlistner)
{
    
}

void*
Led_monitor::worker(void* const data)
{
    localize(data).worker();
    return 0;
}

void
Led_monitor::worker()
{
    cout << "\n Worker thread" << endl;
    int working = 1;
    while(working)
    {
        int size_requests = 0;
        int size_events = 0;
        StateChangeRequest* statechangerequest = 0;  //cannot update data, StateChangeRequest* const statechangerequest cannot point to diff. address
        int notify_value = 0;
        exception* e = 0; //if I make it const exception* e, the destructor will try to update the content of e which would cause error.
        //use static_const before destructing 
        
        {
            unique_lock<mutex> dataLock(dataMonitor);
            cout << "Value of led_monitor working status" << working << endl;
//          No need to check led_monitor->working because its guarded by the stateMonitor.
            while (0 == requests.size() && 0 == events.size())
            {
                dataMonitor.wait(dataLock);
                cout << "Waiting" << endl;
            }
            cout << "waited" << endl;
            cout << "Size of event" << events.size() << endl;
            
            
            size_requests = requests.size();
            size_events = events.size();

            if(0 != size_requests)
            {
                cout << "Size of request list" << requests.size() << endl;
                statechangerequest = *requests.begin();

                //statechangerequest->doSomething();
                if(StateChangeRequest::REQUEST_CLOSE() != statechangerequest)
                {
                    try
                    {
                        cout << "Calling ChangeState" << endl;
                        ChangeStateSync(statechangerequest->commands);
                    }
                    catch(ios_base::failure& failureObj)
                    {
                        cout<<"Error in state change" << failureObj.what() << endl;
                        e = new ios_base::failure(failureObj.what());  
                    }
                    requests.pop_front();
                }
                else
                {
                    working = 0;
//                    e = new runtime_error("Run time error:no events on the list"); 
                }
             }



            if(0 != size_events)
            {
                notify_value = *events.begin();
                events.pop_front();
            }
            
        }
        
        if(0 != statechangerequest && 0 != statechangerequest->ledlistner)
        {   
            if(0 != e)
            {
                statechangerequest->ledlistner->led_monitorresult(statechangerequest->commands,*e);

            }
            else
            {
                statechangerequest->ledlistner->led_monitorresult(statechangerequest->commands,EXCEPTION_HANDLE());
            }
            if(StateChangeRequest::REQUEST_CLOSE() != statechangerequest)
            {
                delete statechangerequest;
                statechangerequest = 0;
            }
        }
        
        LedNotify* notifications = 0;
        if(FILE_MODIFY == notify_value)
        {
            int ifp = open("/sys/class/leds/phy0-led/brightness",O_RDONLY);
            if(0 == ifp)
            {
                throw(ios_base::failure("Unable to open the file"));
            }
            char string[2];
            size_t length = sizeof(string);
            size_t sum = 0;
            int retval = 0;
            
            while(sum != length)
            {
                retval = read(ifp,string,length-sum);
                sum =+ retval;
            }
            
            if(0 == sum)
            {
                throw(ios_base::failure("Unable to read the file"));
            }
            currentState = ('0' == string[0]) ? STATE_OFF : STATE_ON;

            if(!lednotifications.empty())
            {
                notifications = *lednotifications.begin();
                notifications->notifyState(currentState);
            }
        }
        if(FILE_CLOSE == notify_value)
        {
            currentState = STATE_NONE;
            if(!lednotifications.empty())
            {
                notifications = *lednotifications.begin();
                notifications->notifyState(currentState);
                lednotifications.pop_front();
            }
        }
        if(0 == working)
        {
            working = 0;
        }
    }
    
    cout << "exiting worker" << working << endl;
}




void
Led_monitor::ChangeState(Command command, LedListner* ledlistner)
{
   
    StateChangeRequest* statechangerequest = new StateChangeRequest(command,ledlistner);

    if(NULL == statechangerequest)
    {
        
        throw runtime_error("Unable to create a new statechange request");
    }
    {
        unique_lock<mutex> stateLock(stateMonitor);
        if(!working)
        {
            throw runtime_error("Led monitor is not running");
        }
        unique_lock<mutex> dataLock(dataMonitor);
        requests.push_back(statechangerequest);
    }
    dataMonitor.notify_one();
}
void
Led_monitor::ChangeState(Command command)
{
    if (1 != working)
    {
        throw runtime_error("Run time error:service is not running");
    }
//    const char* val;
//    switch(command)
//    {
//        case STATE_OFF:
//        {
////                int* p;
////                p = &request->parameters.commandparams.brightness;
////                File_write("/sys/class/leds/phy0-led/brightness", p, sizeof(int));
//            val = "0";    
//            break;
//        }
//
//        case STATE_ON:
//        {
////                int* p;
////                p = &request->parameters.commandparams.brightness;
//            val = "1";
//            break;
//        }
//            
//        default:
//        {
//            val = 0;
//            // throw exception
//            break;
//        }
//    }
//    file_write("/sys/class/leds/phy0-led/brightness", val, 2); // "0" will write to the file in the form of ASCII instead of binary
    cout<< "Changing the state of led" << endl;
    file_write("/sys/class/leds/phy0-led/brightness", command ? "1" : "0", 2);
}

void 
Led_monitor::ChangeStateSync(Command command)
{
    file_write("/sys/class/leds/phy0-led/brightness", command ? "1" : "0", 2);
}

void Led_monitor::file_write(const char* filename_for_write, const void* array, size_t total)
{
    if(0 != total)
    {
        FILE * ifp = fopen(filename_for_write, "w");
        if(NULL == ifp)
        {
            throw ios_base::failure("Unable to open the file");
        }
        size_t status = fwrite(array, 1, total, ifp);
        if(status != total)
        {
            throw ios_base::failure("Unable to write to the file");
        }
        cout << "Total size write" << status << endl;
        fclose(ifp);
    }
    else
    {
        cout << "Bytes to be written cannot be zero" << endl;
    }
}


void
Led_monitor::Register_fornotification(LedNotify& lednotify)
{
    unique_lock<mutex> stateLock(stateMonitor);
    if(0 != working)
    {
        unique_lock<mutex> datalock(dataMonitor);
        lednotifications.push_back(&lednotify);
    }
}
void
Led_monitor::worker_notify()
{
    int working = 1;
    const int readDescriptor = pipeNotify.getReadDescriptor();
    //struct timeval timeout;
    fd_set readDescriptors;
    fd_set errorDescriptors;
    while(0 != working)
    {
        
        inotify_event* dataptr = (inotify_event*)malloc(sizeof(inotify_event));
        //timeout.tv_sec = 0;
        //timeout.tv_usec = 500000;
        
        FD_ZERO(&readDescriptors);
        FD_ZERO(&errorDescriptors);
        
        /* Indicate interest in monitoring the pipe's read descriptor for readable data or error.
         */
        FD_SET(readDescriptor, &readDescriptors);
        FD_SET(readDescriptor, &errorDescriptors);
        FD_SET(fd, &readDescriptors);
        FD_SET(fd,&errorDescriptors);
        
        int status = ::select(max(readDescriptor,fd)+1,&readDescriptors,0,&errorDescriptors,0);
        //cout << "Value of the status" << status << endl;
        if(-1 != status)
        {
            if(0 == status)
            {
                //timeout occurred
            }
            else
            {
                assert(1 <= status);
                if(FD_ISSET(readDescriptor, &readDescriptors))
                {
                    unique_lock<mutex> datalock(dataMonitor);
                    events.push_back(FILE_CLOSE);
                    working = 0;
                    //cout << "readDescriptor is set" << this->events.size() << endl;
                }
                else if(FD_ISSET(readDescriptor, &errorDescriptors))
                {
                    cout << "Error in setting file descriptor" << endl;
                    unique_lock<mutex> datalock(dataMonitor);
                    events.push_back(FILE_CLOSE);
                    working = 0;
                }
                else if(FD_ISSET(fd, &readDescriptors))
                {
                           cout << "Sizeof inotify %d" << sizeof(inotify_event) << endl;
                    //dataptr->array = &((char*)dataptr)[sizeof(Data)];
                    int sum = 0;
                    int retval = 0;
                    int length = sizeof(inotify_event);


                    cout << "Size of inotify_event sum retval" << sizeof(inotify_event) << "" << sum << retval << endl;


                    while(0 != working && length != sum && (-1 != retval))
                    {
                        cout << "In the while loop worker_notify" << endl;
                        retval = read(fd, &dataptr[sum], length - sum);

                        if(- 1 != retval)
                        {
                            sum += retval;
                            if(sizeof(inotify_event) == sum && 0 < dataptr->len)
                            {
                                length += dataptr->len;
                                dataptr = (inotify_event*) ::realloc(dataptr,length);       
                            }
                        }
                        cout << "Value of sum" << sum << endl;

                    }

                    cout << "Total number of bytes of len field " <<  dataptr->len << endl;
                    unique_lock<mutex> dataLock(dataMonitor);
                    
                    if(-1 != retval)
                    {
                        if(dataptr->mask & IN_MODIFY)
                        {
                            events.push_back(FILE_MODIFY);

                        }
                        if(dataptr->mask & IN_DELETE)
                        {
                            events.push_back(FILE_DELETE);
                        }


                        cout << "Signaling monitor " << events.size() << endl;
                        this->dataMonitor.notify_one();

                    }

                }
                else if(FD_ISSET(fd, &errorDescriptors))
                {
                    unique_lock<mutex> datalock(dataMonitor);
                    events.push_back(FILE_CLOSE);
                    working = 0;
                }

            }
        }
        else
        {
            switch (errno)
            {
                case EBADF:
                {
                  cerr << "The pipe was closed" << endl;
                    
                    /* The pipe was closed, which should be the result of a call to 'close'. This is intentional and therefore no 'EVENT_ERROR' is sent.
                     */
                    working = false;
                    break;
                }
                
                case EINVAL:
                {
                    /* This is the result of an implementation error.
                     */
                    assert(false);
                    break;
                }
                
                case EINTR:
                {
                    /* Do nothing.
                     */
                    break;
                }
                
                default:
                {
                    cerr << "Unhandled select error: " << ::strerror(errno) << endl;
                    break;
                }
            }
            
        }
        
    }
    cout << "Exiting worker notify" << working << endl;
    
}
void
Led_monitor::LedMonitorClose()
{
    unique_lock<mutex> stateLock(stateMonitor);
    
    if(0 != working)
    {
        working = 0;
        const char msg = 0;
        ssize_t written = 1;
        cout << "Write data to the pipe" << endl;
        ssize_t writting = ::write(pipeNotify.getWriteDescriptor(),&msg,written);
        cout << "Number of bytes written" << writting << endl;
        if(writting != written)
        {
            cerr << "Unable to write" << endl;
            pipeNotify.close();
        }
        {
            unique_lock<mutex> dataLock(dataMonitor);
            
            requests.push_back(StateChangeRequest::REQUEST_CLOSE());
        }
        dataMonitor.notify_one();
        
        if(0 != inotify_rm_watch(fd, wd))
        {
           cout << "Error in removing the watch" << endl;
        }
        if(0 != close(fd))
        {
            cout << "Error in closing the file" << endl;
        }
        if(0 != pthread_join(thread_worker,0))
        {
            throw runtime_error("Error in joining thread_worker");
        }
        cout << "joined thread_worker" << endl;
        if(0 != pthread_join(thread_notify,0))
        {
            throw runtime_error("Error in joining thread_notify");
        }
        cout << "joined thread_notify" << endl;
    }
    //no need to destroy the thread because destructure will be deallocate the members when the function exits
}

Led_monitor::~Led_monitor()
{
    try
    {
        LedMonitorClose();
    }
    catch(std::runtime_error& runtimeObj)
    {
        cout << "Failed to stop due to multiple reasons" << runtimeObj.what() << endl;
    }
    catch(std::ios_base::failure& iosbaseObj)
    {
        cout << "Failed to stop due to I/O errors" << iosbaseObj.what() << endl;
    }
}
}
        