/* 
 * File:   Math.cpp
 * Author: romi
 *
 * Created on June 28, 2015, 9:27 PM
 */

#include <string>
#include <list>
#include <iostream>
#include "Math.hpp"
#include "pthread.h"
#include <unistd.h>

using namespace std;

namespace leds {

Math::Math()
{
    Monitor* monitor = new Monitor();
    pthread_t thread;
    pthread_create(&thread,NULL,worker,this);
    working = 1;
    this->monitor = monitor;
    cout << "Math Constructor" << endl;
}

void
Math::addListener(MathListener* mathlistener)
{
    if(0 != mathlistener)
    {
        this->mathlisteners.push_back(mathlistener);
    }
}

void*
Math::worker(void* const data)
{
   
   Math* const math = static_cast<Math*>(data);
   int working = 1;
   while(working)
   {
       unique_lock<mutex> lck(*math->monitor);
       while(1 == math->working && 0 == math->mathrequests.size())
       {
           math->monitor->wait(lck);
           cout << "Waiting" << endl;
       }
       cout << "waited" << endl;
       if(0 != math->mathrequests.size())
       {
            cout << "Size of list " << math->mathrequests.size() << endl;
            MathRequest*  const mathrequest = *math->mathrequests.begin();
            int result = math->add(mathrequest->a,mathrequest->b);
            mathrequest->mathlistener->mathresult(result);
            math->mathrequests.pop_front();
            delete mathrequest;
            
                //mathlistener->mathresult(true);
                
                
                //std::unique_lock<std::mutex> unlck(math->monitor->mtx);
                
       }
       else
       {
           working = 0;
       }
   }
   cout << "exiting" << endl;
   return NULL;
}
Math::MathRequest::MathRequest(int a, int b, MathListener* mathlistener):
a(a),b(b),mathlistener(mathlistener)
{
    
}
int
Math::add(int a, int b)
{
    int result = 0;
    result = a + b;
    cout << "Value of result \n" << result << endl;
    return result;
}
void
Math::add(int a, int b, MathListener* mathlistener)
{
    if(0 != mathlistener)
    {
        MathRequest* mathrequest = new MathRequest(a,b,mathlistener);
        mathrequests.push_back(mathrequest);
        monitor->notify_one();
        
    }
}


}