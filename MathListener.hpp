/* 
 * File:   MathListener.hpp
 * Author: romi
 *
 * Created on July 19, 2015, 7:01 PM
 */

#ifndef MATHLISTENER_HPP
#define	MATHLISTENER_HPP

class MathListener
{
    public: virtual void
    mathresult(int value) = 0;
    
    
};


#endif	/* MATHLISTENER_HPP */

