/*
 * File:   SomeDriver.cpp
 * Author: romi
 *
 * Created on July 17, 2015, 9:17 PM
 */

#include "SomeDriver.hpp"
#include "Math.hpp"
#include <unistd.h>


void SomeDriver::SomeDriverMathListener::mathresult(int value)
{
    cout << "Math result \n" << value << endl;
    somedriver->result = value;
}
SomeDriver::SomeDriverMathListener::SomeDriverMathListener(SomeDriver* driver):
somedriver(driver)
{
    
}
SomeDriver::SomeDriverLedMonitor::SomeDriverLedMonitor(SomeDriver* driver):
somedriver(driver)
{
    
}
SomeDriver::SomeDriver()
{
    this->led_monitor = Led_monitor::create();
    this->math = new Math();
}

SomeDriver*
SomeDriver::create()
{
    return new SomeDriver();
}

void
SomeDriver:: testMath()
{
    MathListener* somedrivermathlistener = new SomeDriverMathListener(this);
    math->add(1,2,somedrivermathlistener);
    math->add(5,2,somedrivermathlistener);
    sleep(1);
}
void
SomeDriver::testLed()
{
    LedListner* somedriverledmonitorlistner = new SomeDriverLedMonitor(this);
    LedNotify* somedriverlednotifylistner = new SomeDriverNotify(this);
    led_monitor->Register_fornotification(*somedriverlednotifylistner);
    try
    {
        led_monitor->ChangeState(STATE_OFF,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_ON,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_OFF,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_ON,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_ON,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_OFF,somedriverledmonitorlistner);
        led_monitor->ChangeState(STATE_ON,somedriverledmonitorlistner);
    }
    catch(invalid_argument& invalidArgument)
    {
        cout << "Invalid argument with message:" << invalidArgument.what() << endl;
    }
    catch(ios_base::failure& ioException)
    {
        cout << "ioException with error message:" << ioException.what() << endl;
    }
    catch (runtime_error& runtimeError)
    {
        cout << "Runtime error occurred:" << runtimeError.what() << endl;
    }
    
    
    
    
    // a) 'ChangeState' is called 6 times 
    
    for (int i = 0; i < 6; ++i)
    {
        try
        {
            led_monitor->ChangeState((Command) i ,somedriverledmonitorlistner);
        }
        catch(invalid_argument& invalidArgument)
        {
            cout << "Invalid argument with message:" << invalidArgument.what() << endl;
        }
    }
    
    // b) 'ChangeState' is called 3 times 
    
    try
    {
        for (int i = 0; i < 6; ++i)
        {
            led_monitor->ChangeState((Command) i ,somedriverledmonitorlistner);
        }
    }
    catch(invalid_argument& invalidArgument)
    {
        cout << "Invalid argument with message:" << invalidArgument.what() << endl;
    }
}
SomeDriver::SomeDriverNotify::SomeDriverNotify(SomeDriver* driver):
somedriver(driver)
{
   
}
void
SomeDriver::stopMathLed()
{
    cout << "Closing ledMonitor" << endl;
    
    if(0 != math)
    {
        delete math;
        math = 0;
    }
    try
    {
        led_monitor->LedMonitorClose();
        led_monitor->LedMonitorClose();
    }
    catch(std::runtime_error& runtimeObj)
    {
        cout << "Failed to stop due to multiple reasons" << runtimeObj.what() << endl;
    }
    catch(std::ios_base::failure& iosbaseObj)
    {
        cout << "Failed to stop due to I/O errors" << iosbaseObj.what() << endl;
    }
}

Pointer<Led_monitor>
SomeDriver::getPointer()
{
    return led_monitor;
}



