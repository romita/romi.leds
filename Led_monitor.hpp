/* 
 * File:   Led_monitor.hpp
 * Author: romi
 *
 * Created on September 14, 2015, 11:29 PM
 */

#ifndef LED_MONITOR_HPP
#define	LED_MONITOR_HPP
#ifdef _cplusplus
extern "C"
{
#endif
#include <iostream>
#include "Monitor.hpp"
#include "LedListner.hpp"
#include "LedNotify.hpp"
#include <sys/inotify.h>
#include <assert.h>
#include <list>
#include "Pointer.hpp"
    
using namespace  std;
namespace leds {
    
class Pipe
{
    public: static const int DESCRIPTOR_INVALID = -1;
    
    /* Index of the read descriptor.
     */
    private: static const unsigned int INDEX_READ = 0;
    
    /* Index of the write descriptor.
     */
    private: static const unsigned int INDEX_WRITE = 1;
    
    /* The pipe descriptors. The value at index '0' is for reading while the value at index '1' is for writing. See http://linux.die.net/man/3/pipe for more information.
     */
    private: int descriptors[2];
    
    private: bool closed;
    
    public:
    Pipe();
    public:
    ~Pipe();
    
    public: void
    close();
    
    public: int
    getReadDescriptor();
    
    public: int
    getWriteDescriptor();
};
    
class ImmutableClass
{
    private: int value;
    
    public: ImmutableClass(int value):
    value(value) {
        //
    }
    
    public: int getValue() const {
        return value;
    }
    
//    public: void reset() {
//        value = 0;
//    }
};

class ImmutableClass2
{
    public: static const int DEFAULT_VALUE = -1;
    
    public: const int value;
    
    public: ImmutableClass2(int value):
    value(value) {
//        this->value = value;
    }
    
    // not necessary
//    public: int getValue() const {
//        return value;
//    }
    
    public: void reset() {
//        value = 0; // compiler error
    }
};

class Led_monitor
{   
    static const int FILE_DELETE = 1;
    static const int FILE_MODIFY = 2;
    static const int FILE_CLOSE = 3;
    typedef struct inotify_event inotify_event;
    private: Led_monitor();
    public:~Led_monitor();
    public: static Pointer<Led_monitor> create();
//    public: explicit Led_monitor(const char* something);
    //public: explicit Led_monitor(int value);
    public: void addListner(LedListner* ledlistner);
    public: void LedMonitorClose();
    private: Monitor dataMonitor;
    private: Monitor stateMonitor;
    private: static void file_write(const char* filename_for_write, const void* array, size_t total);
    public: enum Notification {STATE_CHANGED = 1,PLUG_CHANGED = 2};
    private:Command currentState;
    private: int fd;
    private: int wd;
    public: static const exception& EXCEPTION_HANDLE();
    private:class StateChangeRequest
    {
        public: static StateChangeRequest* REQUEST_CLOSE();
        public: Command commands; //make it private
        public: LedListner* ledlistner;//make it private
        private: StateChangeRequest();
        public: StateChangeRequest(Command commands, LedListner* ledlistner);
        //public: ~StateChangeRequest();
        private: mutable int gottens;
        
        public: Command
        getCommands() const
        {
            gottens++;
            //commands = STATE_OFF;
            
            return commands;
        }
        
        public: void
        setCommands(const Command commands)
        {
            this->commands = commands;
        }
    };
    friend class StateChangeRequest;
    public:static int isEnum(int x);
    //public:int Writecommand_respondlater(Command command, LedListner* ledlistner);
    private: list<LedListner*> ledlistners;
    private: list<StateChangeRequest*> requests;
    private: void
    worker();
    private: static void*
    worker(void* const data);
    private: int working;
    private: pthread_t thread_worker;
    private: pthread_t thread_notify;
    public: void ChangeState(Command command,LedListner* ledlistner);
    public: void ChangeState(Command command);
    public:void Register_fornotification(LedNotify& lednotify);
    private: list<LedNotify*> lednotifications;
    private: list<int> events;
    private: void 
    worker_notify();
    private: static void* 
    worker_notify(void* const context);
    private: static Led_monitor&
    localize(void* const context);
    private: Pipe pipeNotify;
    private: void ChangeStateSync(Command command);
    /*private: Command current_state;*/
};
} // leds
#ifdef _cplusplus
}
#endif

#endif	/* LED_MONITOR_HPP */

