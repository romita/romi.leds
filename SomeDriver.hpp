/* 
 * File:   SomeDriver.hpp
 * Author: romi
 *
 * Created on July 19, 2015, 7:00 PM
 */

#ifndef SOMEDRIVER_HPP
#define	SOMEDRIVER_HPP

#include "Math.hpp"
#include "MathListener.hpp"
#include "Led_monitor.hpp"
#include "Pointer.hpp"
using namespace std;
using namespace leds;
class SomeDriver
{
private: Math* math;
private: Pointer<Led_monitor> led_monitor;
private: SomeDriver();
private: SomeDriver(Math* const math);
private: SomeDriver(Pointer<Led_monitor> const led_monitor);
private: bool status;
private: int result;
private: class SomeDriverMathListener: public virtual MathListener
{
    private: SomeDriver* somedriver;
    public: SomeDriverMathListener(SomeDriver* driver);
    void mathresult(int value);
    void mathrequest(int value)
    {
        
        cout << "Math request received" << value << endl;
    }
};
private: class SomeDriverLedMonitor:public virtual LedListner
{
    private: SomeDriver* somedriver;
    public: SomeDriverLedMonitor(SomeDriver* driver);
    void led_monitorresult(Command command, const exception& e)
    {
        cout << "Result of the led_monitorresult state" << command << endl;
        if(&Led_monitor::EXCEPTION_HANDLE() != &e) //comparing Led_monitor::EXCEPTION_HANDLE() and e which and 2 references might lead to undefined behaviour because its comparing by values
        {
            if (0 != dynamic_cast<const ios_base::failure*>(&e))
            {

                somedriver->led_monitor->ChangeState(command, this);
            }
            else if (0 != dynamic_cast<const runtime_error*>(&e))
            {
    //            runtime_error* runtimeException = dynamic_cast<runtime_error*>(e);
            }
            else
            {
                //
            
            }
        }
        
    }
    
};
private: class SomeDriverNotify:public virtual LedNotify
{
    private: SomeDriver* somedriver;
    public: SomeDriverNotify(SomeDriver* driver);
    void notifyState(Command statechange)
    {
        cout << "Receiving notification for led state change" << statechange << endl;
    }
    void notifyPlug(Command plugchanged)
    {
        cout << "Receiving notification for led plug change" << plugchanged << endl;
    }
};
friend class SomeDriverLedMonitor;
friend class SomeDriverMathListener;
public: static SomeDriver* create();
public: void stopMathLed();
public: void testLed();
public: void testMath();
public: Pointer<Led_monitor> getPointer();
};

#endif	/* SOMEDRIVER_HPP */

