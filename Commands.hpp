/* 
 * File:   Commands.hpp
 * Author: mumpthy
 *
 * Created on January 25, 2016, 2:17 AM
 */

#ifndef COMMANDS_HPP
#define	COMMANDS_HPP

enum Command {STATE_OFF = 0,STATE_ON = 1,STATE_NONE = 2};

#endif	/* COMMANDS_HPP */

