/* 
 * File:   main.cpp
 * Author: romi
 *
 * Created on June 28, 2015, 9:27 PM
 */

#include <string>
#include <list>
#include <iostream>
#include "Math.hpp"
#include "SomeDriver.hpp"
#include <unistd.h>
using namespace std;

/*
 * 
 */

int main(int argc, char** argv) {
    
    try
    {
        SomeDriver* somedriver = SomeDriver::create();
        somedriver->testLed();
        sleep(1);
        somedriver->stopMathLed();
        delete somedriver;
    }
    catch(bad_alloc& exceptionObj)
    {
        cout << "Allocation failed" << exceptionObj.what() << endl;
        throw exceptionObj;
    }
    
    return 0;
}

