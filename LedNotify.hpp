/* 
 * File:   LedMonitor_Notify.hpp
 * Author: romi
 *
 * Created on November 23, 2015, 11:33 PM
 */

#ifndef LEDMONITOR_NOTIFY_HPP
#define	LEDMONITOR_NOTIFY_HPP

#include "Commands.hpp"
namespace leds{
 class LedNotify
    {
        public:virtual void
        notifyState(Command statechange) = 0;
        public:virtual void
        notifyPlug(Command plugchanged) = 0;
        
    };
}
#endif	/* LEDMONITOR_NOTIFY_HPP */

