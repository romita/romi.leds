#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1109843353/Led_monitor.o \
	${OBJECTDIR}/_ext/1109843353/Math.o \
	${OBJECTDIR}/_ext/1109843353/Monitor.o \
	${OBJECTDIR}/_ext/1109843353/SomeDriver.o \
	${OBJECTDIR}/_ext/1109843353/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/leds_cplusplus

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/leds_cplusplus: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/leds_cplusplus ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1109843353/Led_monitor.o: /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1109843353
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/home/mumpthy/NetBeansProjects/LEDS_Cplusplus -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Commands.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedListner.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedNotify.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/MathListener.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Pointer.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.hpp -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1109843353/Led_monitor.o /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.cpp

${OBJECTDIR}/_ext/1109843353/Math.o: /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1109843353
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/home/mumpthy/NetBeansProjects/LEDS_Cplusplus -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Commands.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedListner.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedNotify.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/MathListener.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Pointer.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.hpp -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1109843353/Math.o /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.cpp

${OBJECTDIR}/_ext/1109843353/Monitor.o: /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1109843353
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/home/mumpthy/NetBeansProjects/LEDS_Cplusplus -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Commands.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedListner.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedNotify.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/MathListener.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Pointer.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.hpp -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1109843353/Monitor.o /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.cpp

${OBJECTDIR}/_ext/1109843353/SomeDriver.o: /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1109843353
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/home/mumpthy/NetBeansProjects/LEDS_Cplusplus -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Commands.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedListner.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedNotify.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/MathListener.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Pointer.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.hpp -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1109843353/SomeDriver.o /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.cpp

${OBJECTDIR}/_ext/1109843353/main.o: /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1109843353
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/home/mumpthy/NetBeansProjects/LEDS_Cplusplus -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Commands.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Led_monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedListner.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/LedNotify.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Math.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/MathListener.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Monitor.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/Pointer.hpp -include /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/SomeDriver.hpp -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1109843353/main.o /home/mumpthy/NetBeansProjects/LEDS_Cplusplus/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/leds_cplusplus

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
