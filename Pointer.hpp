#ifndef POINTER_HPP
#define	POINTER_HPP

#include <assert.h>
#include <iostream>
#include <stdexcept>



namespace leds {
    
template <class T>
class Pointer
{
    private: class Counter {
        public: int $count;
        public: T* const $type;
        
        private: Counter();
        
        public: Counter(T* const type): $count(1), $type(type) {
            assert(0 != type);
        }
        
        private: Counter(const Counter& counter);
        
        private: Counter& operator=(const Counter& counter);
    };
    
    private: Counter* $counter;
    
    public: Pointer(): $counter(0) {
        //
    }
    
    public: Pointer(T* const type): $counter(0 != type ? new Counter(type) : 0) {
        /* Do nothing.
         */
    }
    
    public: Pointer(const Pointer& pointer): $counter(pointer.$counter) {
        if (0 != $counter) {
            ++$counter->$count;
        }
    }
    
    public: ~Pointer() {
        release();
    }
    
    public: int count() {
        return 0 != $counter ? $counter->$count : 0;
    }
    
    public: Pointer& operator=(
        const Pointer& pointer) {
        if (this != &pointer) {
            release();

            $counter = pointer.$counter;
            
            if (0 != $counter)
            {
                ++$counter->$count;
            }
        }
        
        return *this;
    }
    
    public: T*
    getTheThing();
    
    public: T*
    operator->() {
        if (0 == $counter)
        {
            throw std::runtime_error("The pointer is null");
        }
    
        return $counter->$type;
    }
    
    private: void release() {
        if (0 != $counter) {
            assert(0 <= $counter->$count);
            
            --$counter->$count;

            if (0 == $counter->$count) {
                assert(0 != $counter->$type);

                delete $counter->$type;
                delete $counter;
            }
        }
    }
};
    
template <class T>
T*
Pointer<T>::getTheThing()
{
    return 0 != $counter ? $counter->$type : 0;
}
    
} /* romi */


#endif	/* POINTER_HPP */

